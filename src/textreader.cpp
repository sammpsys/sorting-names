#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

bool comparename(std::string name1, std::string name2);             //declare comparename function
bool namecheck;                                                     //declare boolean for checking name comparison
std::vector<std::string> sortednames(std::vector<std::string> data);        //sorting function
std::vector<std::string> allnamessorted;                                //vector with sorted names
int main()
{ 
	std::ifstream file; 
	std::string inputString; 
	std::vector<std::string> data;  
	file.open("/mnt/c/Users/SNassi/Documents/Projects/readtext/src/names.txt");         // read in file
	while(file>>inputString)            //reads one string at a time 
		data.push_back(inputString);        //add it to data vector 
	file.close(); 

    allnamessorted=sortednames(data);               //get sorted names
    for (size_t i=0;i<1000;i++)                     // print first names
    std::cout << i << '.' << allnamessorted[i] << std::endl;

} 

std::vector<std::string> sortednames(std::vector<std::string> data){        //sorting algorithm. returns a vector with all names sorted. 
    std::vector<std::string> namessorted;
        for (size_t i=0; i<data.size(); i++){
            std::string temp=data[i];                                   //temporary vectors
            int index=i;                                                // get position of low name
            for (size_t j=0; j<data.size();j++) {
                bool namecheck=comparename(temp,data[j]);               // get out which name is lower
                if (namecheck==0){                          
                    temp=data[j];                                        // save name in temp
                    index=j;                                             // save position in index
                }

            }
            namessorted.push_back(temp);                                //put low name in back of vector
            data.erase((data.begin() + index));                         //remove name from data
        }

        return namessorted;
    }

bool comparename(std::string name1, std::string name2) {        //name comparison 
    int lenname1=name1.length();
    int lenname2=name2.length();
    int len;
    if (lenname1<=lenname2){            //take the shortest name and use it to iterate over the names
        len=lenname1;
    }
    else {
        len=lenname2;
    }

    for (size_t i=0;i<len;i++){
        if (name1[i]==name2[i] && i==(len-1)){            // in case of tie, return the shortest one
            if (lenname1<lenname2){            // if name 1 is shorter, return 1
                return 1;
            }
            else if (lenname2<lenname1){        // if name 2 is shorter, return 0
                return 0;
            }
        }
        if (name1[i]<name2[i]) {
            return 1;               // if name 1 is alphabetically less than name 2 return 1
        }
        else if(name2[i]<name1[i]){
            return 0;               // if name 2 is alphabetically less than name 1 return 1
        }
    }
    return 1;
}
