# Sorting names

Reads a text file containing names and prints the first 1000 in alphabetical order. 

## Table of Contents

- [Requirements](#requirements)
- [Usage](#usage)
- [Maintainers](#maintainers)

## Requirements

Requires `gcc`.

## Usage

Compile and run the file textreader.cpp. No user input is required - the program will read the names from textfile names.txt and prints the first 1000 names in the terminal.

## Maintainers

[Sam Nassiri (@sammpsys)](https://gitlab.com/sammpsys)
